app.current_position = {};
Template.map.rendered=function(){
  app.getGeoLocation()
  app.getCenterLocation()
}

app.getLocation = function(position){
  // console.log(position)
  app.current_position = position.coords;
  gmaps.initialize();
  app.my_location()
}
app.getGeoLocation = function(){
    setTimeout(function(){
        navigator.geolocation.getCurrentPosition(app.getLocation);      
    },0);
}
app.my_location = function(){
  var marker=new google.maps.Marker({
      position:{lat:app.current_position.latitude,lng:app.current_position.longitude},
      // icon:'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/48/Map-Marker-Ball-Azure-icon.png',
      icon:"/pin/blue.png",
      title:"my location"
    });
  marker.setMap(gmaps.map);
}
app.getCenterLocation = function(){
 $('#googleMap').on('dblclick', function(){
      app.latLngDoubleClick = gmaps.map.getCenter(); 
      app.latLngDoubleClick.latitude = app.latLngDoubleClick.lat();
      app.latLngDoubleClick.longitude = app.latLngDoubleClick.lng();
      console.log(app.latLngDoubleClick);
      Router.go("/addshop");        
    });
}
app.createMarker = function(id){
  var mark=Shops.findOne({_id:id});
  console.log(mark);
  // var marker=new google.maps.Marker({
  //     position:{lat:mark.position.latitude,lng:mark.position.longitude},
  //     // icon:'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/48/Map-Marker-Ball-Azure-icon.png',
  //     icon:"http://icons.iconarchive.com/icons/icons-land/vista-map-markers/48/Map-Marker-Ball-Azure-icon.png",
  //     title:mark.shopname
  //   });
  // marker.setMap(gmaps.map);
  var gMarker = new google.maps.Marker({
            position:{lat:mark.positionLat, lng:mark.positionLng },
            // map: gmaps.map,
            title: mark.shopname,
            // flagColor:marker.flagColor,
            icon:"http://icons.iconarchive.com/icons/icons-land/vista-map-markers/48/Map-Marker-Ball-Azure-icon.png",
            // content : marker.content,
            _id : mark._id,
            draggable: false,
            // animation: google.maps.Animation.DROP,
            
        });
  return gMarker;
  // gMarker.setMap(gmaps.map);
}
gmaps={
  map: null,
  markers:[],
  initialize : function(){
    var mapOptions = {
      zoom: 14,
      center:{lat:app.current_position.latitude,lng:app.current_position.longitude},
      // center:{lat:23.106054399999998, lng: 72.59357539999999},
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      panControl : false,
      zoomControl : false,
      overviewMapControl : false,
      mapTypeControl : false,
      scaleControl : false,
      rotateControl : false,
      streetViewControl : false,
      backgroundColor: 'white',
      disableDoubleClickZoom: true 
    };
    this.map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
    // $('#googleMap').on('dblclick', function(){
    //   app.latLngDoubleClick =gmaps.map.getCenter(); 
    //   console.log(app.latLngDoubleClick);
    //   // Router.go("/addshop");        
    // });
    $("#centerbutton").on('click',function(){
      position={lat:app.current_position.latitude,lng:app.current_position.longitude};
      gmaps.map.panTo(position);
    });
  },
  addmarker : function(id){
    var marker=app.createMarker(id)
    this.markers.push(marker);
    // marker.setMap(gmaps.map);
  }
}
