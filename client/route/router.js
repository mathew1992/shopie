Router.configure({
 layoutTemplate: "main_layout"
});
// Router.map(function () {
// 	this.route("home",{
//     path:"/",
//     template:"main_layout",
//     yieldTemplates:{
//       'navbar': {to: 'menu'}
//     }            
//   });
	// this.route("login",{
	// 	path: '/login',
	// 	template: "login"
	// });
// })	
Router.map(function () {
	this.route("home",{	
		path: '/',
		template: "home", 
	});
	this.route("login",{
		path: '/login',
		template: "login"
	});
	this.route("register",{
		path: '/register',
		template: "register", 
	});
	this.route("userhome",{
		path: '/userhome',
		template: "user_layout",
		onBeforeAction:function(){
			var currentUser =Meteor.userId();
			if(currentUser){
			this.next();
			}
			else{
			this.render("login");
			}
 		},
		yieldTemplates:{
			'left_slider': {to: 'left_slider'},
			'right_slider': {to: 'right_slider'},
			'map': {to: 'center_body'},
		},
	});
	this.route("shopkeeper_layout",{
		path: '/shopkeeper_layout',
		template: "shopkeeper_layout",
		yieldTemplates:{
			'shopleft_slider': {to: 'left_slider'},
			'shopright_slider': {to: 'right_slider'},
		}   
		
	});
	this.route("shopadd_item",{
		path: '/shopkeeper_layout/shopadd_item',
		template: "shopkeeper_layout",
		yieldTemplates:{
			'shopadd_item': {to: 'add_item'},
		}   
		
	});
	this.route("product_gallery",{
		path: '/shopkeeper_layout/product_gallery',
		template: "shopkeeper_layout",
		yieldTemplates:{
			'product_gallery': {to: 'product_gallery'},
		}   
		
	});
	this.route("signin",{
		path: '/signin',
		template: "signin", 
	});
	this.route("addshop",{
		path: '/addshop',
		template: "addshop", 
	});
});