Template.register.helpers({

});
Template.register.events({
	"click #register": function(){
        var name=$('[name=name]').val();
		var email= $('[name=email]').val();
		var password= $('[name=password]').val();
        var type= $('[name=dropdown]').val();
        var object= new User.Model();
        object.email=email;
        object.profile.email=email;
        object.profile.name=name;
        object.profile.type=type;
        object.password=password;

		if(isEmail(email)&&isValidPassword(password)&&isNotEmpty(name)&&isNotEmpty(type)){
			Accounts.createUser(object);
		}
		
		Router.go("/userhome");
	},
    "click #dropdown":function(){
         $('.ui.dropdown').dropdown();
    }
});
isEmail = function(value) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(value)) {
        return true;
    }
    console.log('Please enter a valid email address.');
    return false;
};
isNotEmpty = function(value) {
    if (value && value !== ''){
        return true;
    }
    console.log('Please fill in all required fields.');
    return false;
};
isValidPassword = function(password) {
    if (password.length < 6) {
        console.log('Your password should be 6 characters or longer.');
        return false;
    }
    return true;
};