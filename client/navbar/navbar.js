Template.navbar.helpers({
	items: function(){
	    return Meteor.user().profile.name;
	}
});
Template.navbar.events({
	"click #logout":function(event){
		event.preventDefault();
		Meteor.logout();	
		Router.go("/");
	},
	"click .rightsidemenu":function(){
		$('.ui.right.sidebar').sidebar('toggle');
	},
	"click .leftsidemenu":function(){
		$('.ui.left.sidebar').sidebar('toggle');
	},
});