Template.addshop.events({
	"click #dropdown":function(){
         $('.ui.dropdown').dropdown();
    },
    "click #checkshop":function(){
         $('.ui.checkbox').checkbox();
    },
    "click #addshop":function(){
    	var shopname=$("#shopname").val();
    	var shoptype=$("#shoptype").val();
    	var shopowner=$("#shopowner").val();
    	var positionLat= app.latLngDoubleClick.latitude;
        var positionLng= app.latLngDoubleClick.longitude;
    	// console.log(shopowner);
    	var object = new Shops.Model();
    	object.shopname=shopname;
    	object.shoptype=shoptype;
    	object.shopowner=shopowner;
    	object.positionLat=positionLat;
        object.positionLng=positionLng;
    	var id = Shops.insert(object);
    	console.log(id);
    	gmaps.addmarker(id);
    	Router.go("/userhome");
		}
});