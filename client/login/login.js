Template.login.events({
  "click #userlogin": function()
  {
		var email= $('[name=email]').val();
		var password= $('[name=password]').val();
		if (isEmail(email)&&isNotEmpty(password)) {
		Meteor.loginWithPassword(email,password,function(error){
				console.log(error);
		});}
		Router.go("/userhome");
	}
});
isEmail = function(value) 
{
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (filter.test(value)) {
    return true;
  }
  console.log('Please enter a valid email address.');
  return false;
};
isNotEmpty = function(value) 
{
  if (value && value !== ''){
    return true;
  }
  console.log('Please fill in all required fields.');
  return false;
};