Shops = new Meteor.Collection("shops");
Shops.Model = function(){
	var model = {};
	model.shopname = "";
	model.shoptype = "";
	model.shopowner = "";
	model.positionLat= "";
    model.positionLng= "";

	return model;
}
